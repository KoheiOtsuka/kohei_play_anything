require File.expand_path(File.dirname(__FILE__) + '/../chatwork/chat_work_api')
require File.expand_path(File.dirname(__FILE__) + '/../google_drive/google_drive_api')
require 'bundler/setup'
Bundler.require


#google driveセッション開始
google_api_obj = GoogleDriveApi.new()
#対象のスプレッドシートにアクセス
google_api_obj.init_spread_sheet("1a6u8Sin5XGVaXRi-u4BhTgfn8ePTaYLgt24-TZHQm8U",0)


#chatworkapiを宣言
chatwork_api_obj = ChatWorkApi.new("rooms/" , "post", ids = {:room_id => 31510151} )


#採用サイトのアカウント情報
EMAIL = "s-recruit@temona.co.jp"
PASS = "temonacamp"


# ブラウザ起動
# :chrome, :firefox, :safari, :ie, :operaなどに変更可能
driver = Selenium::WebDriver.for :firefox
#ログイン処理
driver.navigate.to "http://recruit.tech-camp.in/companies/sign_in"
driver.find_element(:css, '#company_email').send_keys(EMAIL)
driver.find_element(:css, '#company_password').send_keys(PASS)
driver.find_element(:css, '.submit > input:nth-child(1)').click

#要素の取得idとschool
1.times do |time|
	driver.find_element(:css, ".nav-tabs > li:nth-child(#{time+1}) > a:nth-child(1)").click
	ws = google_api_obj.ws_obj#対象のシート
	ids_arry = []#すでにスプレッドシートに登録してあるid
	ws.list.each{|record| ids_arry << record["id"]}
	agent = Mechanize.new
	page = Mechanize::Page.new URI.parse(driver.current_url), [], driver.page_source, 200, agent
	page.links_with(:href => /companies\/students\/\d+/).each do |link|
		student_id = link.href.gsub("\/companies\/students\/","")
		student_schol = link.text.gsub(/(\s)/,"").delete("プロフィールを見る")
		if ids_arry.include?(student_id)#すでに登録されている
			next
		else#新規
			ws.list.push({"id" => student_id, "school" => student_schol})
			# chatwork_api_obj.send_chat("就活techcampに新規学生が登録されました id = #{student_id}")
		end
	end
	ws.save
end
driver.quit