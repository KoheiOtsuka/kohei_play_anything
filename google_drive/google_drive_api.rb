require File.expand_path(File.dirname(__FILE__) + '/../api')
require 'google_drive'
class GoogleDriveApi < Api
	attr_accessor :session, :ws_obj
	CLIENT_ID = "1011372968392-kg98bh4jj4rr7imbfe38ulct8h5qpdab.apps.googleusercontent.com"
	CLIENT_SECRET = "hBQIegDRQhMEiXzJUQ3oO2WU"
	REFRESH_TOKEN = "1/Ma1cnCxLN2_VvFFHA_-GQ4fX7tAjaPpVLyvZzX7V4MVIgOrJDtdun6zK6XiATCKT"
	def initialize()
		@session = GoogleDriveApi.login
	end
	def self.login
		client = OAuth2::Client.new(
    CLIENT_ID, CLIENT_SECRET,
    :site => "https://accounts.google.com",
    :token_url => "/o/oauth2/token",
    :authorize_url => "/o/oauth2/auth")

		# refresh_tokenでログイン
		auth_token = OAuth2::AccessToken.from_hash(client,{:refresh_token => REFRESH_TOKEN, :expires_at => 3600})
		auth_token = auth_token.refresh!

		# ログイン完了！
		session = GoogleDrive.login_with_oauth(auth_token.token)
	end

	def init_spread_sheet(spreadsheet_key,sheet_num)
		#インスタンス生成
		@ws_obj = session.spreadsheet_by_key(spreadsheet_key).worksheets[sheet_num]
	end
end


# hash = {"record" => { "id" => "10", "score" => "11", "school" => "12", "year" => "13" }}

# p ws.list.keys
# ws.list.each{|t| p t}

# ws.list.push(
#              hash["record"].each do |key,val|
#              	 ["#{key}" => "#{val}"]
#              end
#              )
# ws.save
# for row in 1..ws.num_rows
#   for col in 1..ws.num_cols
#     p ws[row, col]
#   end
# end


# api_obj = GoogleDriveApi.new()
# api_obj.init_spread_sheet("1a6u8Sin5XGVaXRi-u4BhTgfn8ePTaYLgt24-TZHQm8U",0)
# api_obj.ws_obj.list.each {|list| p list}
