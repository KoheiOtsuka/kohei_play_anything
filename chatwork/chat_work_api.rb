require File.expand_path(File.dirname(__FILE__) + '/../api')
class ChatWorkApi < Api
	attr_accessor :end_point, :http_method, :ids
	@@ssl = true
	@@base_url = "https://api.chatwork.com/v1/"
	@@api_token = "da539106069c911db8a34ba2a1b5d33b"
	@@header = {'X-ChatWorkToken' => @@api_token}
	@@general_info = {:ssl => @@ssl, :header => @@header}
	def initialize(end_point,http_method, ids = {})
		@end_point = end_point
		@http_method = http_method
		@ids = ids
	end
	def get_my_accout_info#自分自身の情報を取得
	end
	def get_my_unread_things#自分の未読数、未読To数、未完了タスク数を返す
	end
	def get_my_tasks#自分のタスク一覧を取得する。
	end
	def get_my_friends#自分のコンタクト一覧を取得
	end
	def get_my_rooms#自分のチャット一覧の取得
	end
	def create_new_room#グループチャットを新規作成
	end
	def change_room#チャットの名前、アイコンをアップデート
	end
	def delete_or_leave_room#グループチャットを退席/削除する
	end
	def get_members_of_room#チャットのメンバー一覧を取得
	end
	def update_member_of_room#チャットのメンバーを一括変更
	end
	def get_latest_100s_mgs#チャットのメッセージ一覧を取得。パラメータ未指定だと前回取得分からの差分のみを返します。(最大100件まで取得)
	end
	def send_chat(msg)#チャットを送る
		parameter = {:body => msg.to_s}
		send("general_#{http_method}", URI(@@base_url + end_point + ids[:room_id].to_s + "/messages" ) , {:ssl => @@ssl, :header => @@header, :data => parameter} )
	end
	def get_msg_info#メッセージ情報を取得
	end
	def get_tasks_of_room#チャットのタスク一覧を取得 (※100件まで取得可能
	end
	def make_task#チャットに新しいタスクを追加
	end
	def get_task_info#タスク情報を取得
	end
	def get_file_info#ファイル情報を取得
	end
end



#p ChatWorkApi.new("rooms/" , "post", ids = {:room_id => 21374998} ).send_chat("how are yuodoing")