module Network
  def general_get(uri, params = {})
    https = Net::HTTP.new(uri.host, uri.port)
    if params[:ssl] == true
	    https.use_ssl = true#sslを有効にする。defaultはfalse
	    https.verify_mode = OpenSSL::SSL::VERIFY_NONE
    end
    body = params[:body] if params[:body]
    return https.get(uri.path,params[:header])
  end
  def general_post(uri, params ={})
  	p uri.request_uri
  	p params
    post_obj = Net::HTTP::Post.new(uri.request_uri, initheader = params[:header])
    post_obj.set_form_data(params[:data]) if params[:data]

    https = Net::HTTP.new(uri.host, uri.port)
    if params[:ssl] == true
	    https.use_ssl = true
	    https.verify_mode = OpenSSL::SSL::VERIFY_NONE
	  end
    https.start do |h|
      response = h.request(post_obj)
    end
  end
  def general_put
  end

  def general_delete
  end
end